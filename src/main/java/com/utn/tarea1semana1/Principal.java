/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana1;

import com.utn.utilitarios.Utilitario;

/**
 *
 * @author fabri
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean cerrar = false;
        Ejercicios ejercicio = new Ejercicios();
        do{
            int opcion = Utilitario.capturaValorEntero("Digite 1 para ver su Numero de la Suerte "
                    + "\nDigite 2 para ver si un numero es perfecto "
                    + "\nDigite 3 para adivinar un numero "
                    + "\nDigite 4 para ejercicio de rimas "
                    + "\nDigite 5 para base de choferes "
                    + "\nDigite 6 para salir");
            if(opcion==1){
                ejercicio.numeroSuerte();
            }else if(opcion==2){
                ejercicio.numeroPerfecto();
            }else if(opcion==3){
                ejercicio.adivinar();
            }else if(opcion==4){
                ejercicio.rimas();
            }else if(opcion==5){
                ejercicio.choferes();
            }else if(opcion==6){
                System.out.println("Gracias por usar el programa.");
                cerrar = true;
            }else{
                System.out.println("Ingrese un valor valido");
            }
            
        }while(cerrar == false);
    }
    
}
