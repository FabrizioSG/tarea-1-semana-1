/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana1;

import com.utn.utilitarios.Utilitario;
import java.util.Random;

/**
 *
 * @author fabri
 */
public class Ejercicios {
    
    public void numeroSuerte(){
        int anno = Utilitario.capturaValorEntero("Ingrese el año de nacimiento");
        int mes = Utilitario.capturaValorEntero("Ingrese el mes");
        int dia = Utilitario.capturaValorEntero("Ingrese el dia");
        int numFinal = anno + mes + dia;
        int numSuerte = 0;
        String numero = String.valueOf(numFinal);
        for(int i=0; i<numero.length(); i++){
            char temporal = numero.charAt(i);
            numSuerte += Integer.parseInt(String.valueOf(temporal));
        }
        System.out.println(numSuerte);
    }
    
    public void numeroPerfecto(){
        int numero = Utilitario.capturaValorEntero("Digite el numero");
       
        String divisores = "";
        int numFinal = 0;
        for (int i=1; i<numero; i++){
            if(numero % i == 0){
                divisores += i;
            }
        }
        for(int x=0; x<divisores.length(); x++){
            char temporal = divisores.charAt(x);
             numFinal += Integer.parseInt(String.valueOf(temporal));
        }
        if(numFinal == numero){
            System.out.println("El numero es perfecto");
        }else{
            System.out.println("El numero NO es perfecto");        
        }
    }
    
    public void adivinar(){
        boolean fin = false;
        int intentos = 0;
        Random oRandom = new Random();
        
        int ganador = oRandom.nextInt(21);
        
        do{
            int numUsuario = Utilitario.capturaValorEntero("Digite un numero entre 0 y 20");
            if(numUsuario == ganador){
                System.out.println("Numero ganador!");
                fin = true;
            }else if(numUsuario < ganador){
                System.out.println("El numero es mayor");
                intentos++;
                System.out.println("Intentos restantes: " + (5-intentos));
            }else if(numUsuario > ganador){
                System.out.println("El numero es menor");
                intentos++;
                System.out.println("Intentos restantes: " + (5-intentos));
            }
        }while(fin == false & intentos <= 5);
    }
    public void rimas(){
        String palabra1 = Utilitario.capturarValorString("Digite la palabra 1");
        String palabra2 = Utilitario.capturarValorString("Digite la palabra 2");
        
        if(palabra1.substring(palabra1.length()-3, palabra1.length()).equals
        (palabra2.substring(palabra2.length()-3, palabra2.length()))){
            
            System.out.println("Las palabras riman");
            
        }else if((palabra1.substring(palabra1.length()-2, palabra1.length()).equals
        (palabra2.substring(palabra2.length()-2, palabra2.length())))){
            
            System.out.println("Las palabras riman un poco");
            
        }else{
            
            System.out.println("Las palabras NO riman");
        }
    }
    
    public void choferes(){
        String[][] choferes = new String[3][4];
        choferes[0][0] = "207460510";
        choferes[0][1] = "Fabrizio";
        choferes[0][2] = "Carga ancha";
        choferes[0][3] = "No peligrosa";
        
        choferes[1][0] = "208890223";
        choferes[1][1] = "Pedro";
        choferes[1][2] = "Carga pesada";
        choferes[1][3] = "Peligrosa";
        
        choferes[2][0] = "204840492";
        choferes[2][1] = "Carlos";
        choferes[2][2] = "Carga liviana";
        choferes[2][3] = "NO peligrosa";
        
        String cedula = Utilitario.capturarValorString("Digite la cedula del chofer: ");
        String datos = "";
        for(int i = 0; i<choferes.length; i++){
            if(choferes[i][0].equals(cedula)){
                int fila = i;
                datos = "Cedula: " + choferes[fila][0] + "\nNombre: " +  choferes[fila][1] + 
                "\nTipo de carga: " +  choferes[fila][2] + "\nPeligrosa: " +  choferes[fila][3];
            }else{
                datos = "Chofer no se encuentra en la base de datos";
            }
        }
        
        System.out.println(datos);
    }
    
    
    
}
