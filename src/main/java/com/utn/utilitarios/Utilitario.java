/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.utilitarios;

import java.util.Scanner;

/**
 *
 * @author mainoralonso
 */
public class Utilitario {
    
    public static int capturaValorEntero(String pTextoPeticion){
        int valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true){
            try{
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextInt();
                continuar = false;
            }catch (Exception e){
                System.out.println("Valor ingresado incorrecto, debe digitar un valor entero!!");
            }
        }
        return valorRetorno;
    }
    
    public static int capturaValorEntero(String pTextoPeticion, int pValorMinimo){
        int valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true){
            try{
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextInt();
                if (valorRetorno >= pValorMinimo){                
                    continuar = false;
                }else{
                    System.out.println("El valor debe ser mayor a " + pValorMinimo);
                }
            }catch (Exception e){
                System.out.println("Valor ingresado incorrecto, debe digitar un valor entero!!");
            }
        }
        return valorRetorno;
    }
               
    public static int capturaValorEntero(String pTextoPeticion, 
                                         int pValorMinimo,
                                         int pValorMaximo){
        int valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true){
            try{
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextInt();
                if (valorRetorno >= pValorMinimo){                
                    continuar = false;
                }else{
                    System.out.println("El valor debe ser mayor a " + pValorMinimo);
                }
                
                if (valorRetorno <= pValorMaximo){                
                    continuar = false;
                }else{
                    System.out.println("El valor debe ser menor a " + pValorMaximo);
                }
            }catch (Exception e){
                System.out.println("Valor ingresado incorrecto, debe digitar un valor entero!!");
            }
        }
        return valorRetorno;
    }
    
    public static double capturaValorDouble(String pTextoPeticion){
        double valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true){
            try{
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextDouble();
                continuar = false;
            }catch (Exception e){
                System.out.println("Valor ingresado incorrecto, debe digitar un valor double!!");
            }
        }
        return valorRetorno;
    }
    
    public static String capturarValorString(String pTextoPeticion){
        String retorno = "";
        Scanner otroTeclado = new Scanner(System.in);
        System.out.println(pTextoPeticion);
        retorno = otroTeclado.nextLine();
        return retorno;
    }
}
